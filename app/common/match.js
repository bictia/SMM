const db = require("../models/firebase");

class Match {

    static getMatch(publicationId) {
        return new Promise((resolve, rejected) => {
            db.getById('match', publicationId)
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    rejected(error);
                });
        });
    }

    static updateMatch(publicationId, uid) {
        return new Promise((resolve, rejected) => {
            Match.getMatch(publicationId)
                .then((response) => {
                    if(response) {
                        if(!(response.uid.includes(uid))) {
                            response.uid.push(uid)
                        }
                    } else {
                        response = {uid: [uid]}
                    }
                    db.update('match', publicationId, response)
                    .then((success) => {
                        resolve(success);
                    })
                    .catch((error) => {
                        rejected(error);
                    });
                });
        });
    }

}

module.exports = Match;