const db = require('../models/firebase');

class Publication {

    static save(data){
        return new Promise((resolve, rejected) => {
            db.insertWithOutId('publications', data)
                .then((response) => {
                    db.getById('publications', response.id)
                    .then((document) => {
                        db.update('publications', response.id, {"ref": document.uid + '/' + response.id})
                        .then(() => resolve(response.id))
                    })
                })
                .catch((error) => {
                    rejected(error);
                });
        });
    

    }

    static getPublication(id) {
        return new Promise((resolve, rejected) => {
            db.getById('publications', id)
            .then((response) => {
                resolve(response);
            })
            .catch((error) => {
                rejected(error);
            });
        });
    }

    static getPublications() {
        return new Promise((resolve, rejected) => {
            db.getAll('publications')
            .then((response) => {
                resolve(response);
            })
            .catch((error) => {
                rejected(error);
            });
        });
    }

    static getUserPublications(uid) {
        return new Promise((resolve, rejected) => {
            db.getByParam('publications', "uid", uid)
            .then((response) => {
                resolve(response);
            })
            .catch((error) => {
                rejected(error);
            });
        });
    }

    static deletePublication(docuentId) {
        return new Promise((resolve, rejected) => {
            db.delete('publications', docuentId)
            .then((response) => {
                resolve(response);
            })
            .catch((error) => {
                rejected(error);
            });
        });
    }

    static updateData(documentId, info){
        return new Promise( (resolve,rejected) => {
            db.update('publications',documentId,info)
            .then( (response) => {
                resolve(response);
            }).catch( (error) => {
                rejected(error);
            });
        });

    }

}
    
module.exports = Publication;