const admin = require('../private/firebaseInitializer');
const auth = admin.auth();

class UserAuth {

    static createUser(kwargs) {
        return new Promise((resolve, rejected) => {
            auth.createUser(kwargs)
            .then((response) => {resolve(response)})
            .catch((error) => {rejected(error)});
        });
    }

    static updateUser(uid, kwargs) {
        return new Promise((resolve, rejected) => {
            auth.updateUser(uid, kwargs)
            .then((response) => {resolve(response)})
            .catch((error) => rejected(error));
        });
    }

    static deleteUser(uid) {
        return new Promise((resolve, rejected) => {
            auth.deleteUser(uid)
            .then((response) => {resolve(response)})
            .catch((error) => {rejected(error)});
        });
    }

    static getUser(uid) {
        return new Promise((resolve, rejected) => {
            auth.getUser(uid)
            .then((response) => {resolve(response)})
            .catch((error) => {rejected(error)});
        });
    }

}

module.exports = UserAuth;