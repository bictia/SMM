const db = require('../models/firebase');

class UserFirestore {

    static getUser(uid) {
        return new Promise((resolve, rejected) => {
            db.getById("users", uid)
            .then((response) => {resolve(response)})
            .catch((error) => {rejected(error)});
        });
    }

    static updateUser(uid, kwargs) {
        return new Promise((resolve, rejected) => {
            db.update("users", uid, kwargs)
            .then((response) => {resolve(response)})
            .catch((error) => {rejected(error)});
        });
    }

}

module.exports = UserFirestore;
