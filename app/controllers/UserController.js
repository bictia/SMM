const express = require('express');
const route = express.Router();

const UserFirestore = require('../common/userFirestore');
const UserAuth = require('../common/userAuth');

route.get('/firestore/:uid', async (request, response) => {
    response.send(await UserFirestore.getUser(request.params.uid));
});

route.put('/firestore/:uid', async (request,response) => {
    response.send(await UserFirestore.updateUser(request.params.uid, request.body));
});

route.get('/auth/:uid', async (request, response) => {
    response.json(await UserAuth.getUser(request.params.uid));
});

route.post('/auth', async (request, response) => {
    response.send(await UserAuth.createUser(request.body));
});

route.put('/auth/:uid', async (request, response) => {
    response.send(await UserAuth.updateUser(request.params.uid, request.body));
});

route.delete('/auth/:uid', async (request, response) => {
    response.send(await UserAuth.deleteUser(request.params.uid));
});

module.exports = route;