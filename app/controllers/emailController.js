const express = require('express');
const route = express.Router();
const Email = require('../common/email');

route.post('/send',async(request,response) => {
    response.send(await Email.sendEmail(request.body));
});

module.exports = route;
