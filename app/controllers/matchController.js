const express = require("express");
const route = express.Router();
const Match = require("../common/match");

route.get('/:publicationId', async (request, response) => {
    response.json(await Match.getMatch(request.params.publicationId));
});

route.put('/:publicationId', async (request, response) => {
    response.send(await Match.updateMatch(request.params.publicationId, request.body.uid));
});

module.exports = route;