const express = require('express');
const route = express.Router();
const Publication = require('../common/publication');

route.post('/', async (request,response) => {
    response.json(await Publication.save(request.body));
});

route.put('/:id',async(request,response) => {
    response.send(await Publication.updateData(request.params.id, request.body));
        
});

route.get('/', async (request, response) => {
    response.send(await Publication.getPublications());
});

route.delete('/', async (request, response) => {
    response.json(await Publication.deletePublication(request.body.id));
});

route.get('/:uid', async (request, response) => {
    response.json(await Publication.getUserPublications(request.params.uid));
});

module.exports = route;
