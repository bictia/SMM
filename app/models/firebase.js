const admin = require('../private/firebaseInitializer');

const db = admin.firestore();

function FirebaseModel()
{

}

FirebaseModel.getAll = function(collection){

    return new Promise( (resolve,rejected) => {
        db.collection(collection).get()
            .then( (responses) => {
                let table = [];
                responses.forEach( (response) => {
                    table.push(response.data())
                });
                resolve(table);
            }).catch( (error) => {
                rejected(error);
            })
    });

}

FirebaseModel.getById = function(collection ,documentId) {

    return new Promise((resolve, rejected) => {
        db.collection(collection).doc(documentId).get()
        .then((response) => {
            resolve(response.data());
        })
        .catch((error) => {
            rejected(error);
        });
    });
}
    

FirebaseModel.getByParam = function(collection, param, value) {

    return new Promise((resolve,rejected) => {
        db.collection(collection).where(param, '==', value)
        .get()
        .then((responses) => {
            let table = [];
            responses.forEach((response) => {
                table.push(response.data());
            });
            resolve(table);
        })
        .catch((error) => {
            rejected(error);
        })
    });
}

//Metodos de Administracion de Datos

 FirebaseModel.insert = function (collection, documentId, document) {

    return new Promise((resolve, rejected) => {
        db.collection(collection).doc(documentId).set(document)
        .then((response) => {
            resolve(response);
        })
        .catch((error) => {
            rejected(error);
        })
    });
}

FirebaseModel.insertWithOutId = function(collection, document) {
    return new Promise((resolve, rejected) => {
        db.collection(collection).add(document).
        then((response) => {
            resolve(response);
        })
        .catch((error) => {
            rejected(error);
        });
    });
}

FirebaseModel.delete = function(collection, documentId){

    return new Promise((resolve, rejected) => {
        db.collection(collection).doc(documentId).delete()
        .then((response) => {
            resolve(response);
        })
        .catch((error) => {
            rejected(error);
        })
    });
}
    

FirebaseModel.update = function (collection, documentId, information) {

    return new Promise((resolve, rejected) => {
        db.collection(collection).doc(documentId).set(information,{merge: true})
        .then((response) => {
            resolve(response);
        })
        .catch((error) => {
            rejected(error);
        })
    });
}

module.exports = FirebaseModel;