const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const puerto = process.env.PORT || 3000;

/*app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});*/

let userController = require('./app/controllers/UserController');
let publicationController = require('./app/controllers/publicationController');
let matchController = require('./app/controllers/matchController');
let emailController = require('./app/controllers/emailController');


app.use(bodyParser.json());
app.use(cors());

app.use('/user',userController);
app.use('/publications',publicationController);
app.use('/match', matchController);
app.use('/email',emailController);

app.get('/',(request,response) => {
    response.send('SMM');
});

app.listen(puerto,() => {
    console.log('Corriendo en el  puerto: ' + puerto);
});